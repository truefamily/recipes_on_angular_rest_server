<?php
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
    header('Access-Control-Allow-Origin : *');
    header('Access-Control-Allow-Methods : POST, GET, OPTIONS, PUT, PATCH, DELETE');
    header('Access-Control-Allow-Headers : Content-Type, Accept, Authorization, X-Requested-With');
    exit;
}
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS, HEAD');
header('Access-Control-Allow-Headers: Content-Type, Accept, Authorization, X-Requested-With');
require 'vendor/autoload.php';
require_once 'lib/mysql.php';
require_once 'dao/ingridients.php';
require_once 'dao/measures.php';
require_once 'dao/tags.php';
require_once 'dao/utils.php';
//require_once 'lib/ChromePhp.php';

// GET методы
Flight::route('GET /recipes', function(){
    getAllRecipes();
});
Flight::route('GET /recipes/last/@num', function($num){
    getLastRecipes($num);
});
Flight::route('GET /recipes/approving', function(){
    getApprovingRecipes();
});
Flight::route('GET /recipes/approving/@id', function($id){
    //TODO getApprovingRecipe($id);
});
Flight::route('GET /recipes/rejecting', function(){
    getRejectingRecipes();
});
Flight::route('GET /recipes/rejecting/@id', function($id){
    //TODO getRejectingRecipe($id);
});
Flight::route('GET /recipes/@id:[0-9]+', function($id){
    getRecipe($id);
});
Flight::route('GET /recipes/total', function(){
    getRecipeCount();
});
Flight::route('GET /measures', function(){
    getMeasures();
});
Flight::route('GET /search/recipes/@searchWord', function($searchWord){
    searchRecipesByTag($searchWord);
});

// POST методы


// PUT методы
Flight::route('PUT /recipes', function(){
    addRecipe();
});

// PATCH методы
Flight::route('PATCH /recipes', function(){
    editRecipe();
});
Flight::route('PATCH /recipes/approving/@id', function($id){
    approveRecipe($id);
});
Flight::route('PATCH /recipes/rejecting/@id', function($id){
    rejectRecipe($id);
});

//DELETE методы
Flight::route('DELETE /recipes/@id', function($id){
    deleteRecipe($id);
});

/**
 * @param $id
 */
function getRecipe($id) {
    $sql = "SELECT * FROM recipe WHERE id = $id";
    $db = connect_db();
    $result = $db->query($sql);
    $recipe=$result->fetch_object();
    $recipeTags=getRecipeTags($recipe->id);
    $recipeIngridients=getRecipeIngridients($recipe->id);
    echo json_encode(fillRecipe($recipe, $recipeTags, $recipeIngridients));
};

/**
 * @param $id
 */
function getRecipeCount() {
    $sql = "SELECT COUNT(id) AS count FROM recipe WHERE status = 1";
    $db = connect_db();
    $result = $db->query($sql);
    $count=$result->fetch_object();
    echo $count->count;
};

function searchRecipesByTag($searchWord) {
    $db = connect_db();
    // TODO скорректировать
    $sqlSearch = "SELECT recipe.* FROM tag, recipe WHERE tag.recipeId = recipe.id AND name LIKE '$searchWord'";
    $result = $db->query($sqlSearch) or trigger_error($db->error."[$sqlSearch]");
    makeRecipes($result);
}

/**
 *
 */
function getAllRecipes() {
    $sql = 'SELECT * FROM recipe WHERE status = 1';
    $db = connect_db();
    $result = $db->query($sql) or trigger_error($db->error."[$sql]");
    makeRecipes($result);
};

/**
 * @param $limit
 */
function getLastRecipes($limit) {
    $sql = "SELECT title FROM recipe WHERE status = 1 ORDER BY id DESC LIMIT $limit";
    $db = connect_db();
    $result = $db->query($sql) or trigger_error($db->error."[$sql]");
    $myArray = array();
    while($row = $result->fetch_assoc()) {
        $myArray[] = $row;
    }
    echo json_encode($myArray);
};

/**
 *
 */
function getApprovingRecipes() {
    $sql = 'SELECT * FROM recipe WHERE status = 0';
    $db = connect_db();
    $result = $db->query($sql) or trigger_error($db->error."[$sql]");
    makeRecipes($result);
};

/**
 *
 */
function getRejectingRecipes() {
    $sql = 'SELECT * FROM recipe WHERE status = -1';
    $db = connect_db();
    $result = $db->query($sql) or trigger_error($db->error."[$sql]");
    makeRecipes($result);
};

function makeRecipes(mysqli_result $result) {
    $recipes = array();
    $recipeIds = array();
    while($row = $result->fetch_assoc()) {
        $recipeIds[] = $row['id'];
        $recipes[] = $row;
    }
    $recipeTags=getTags($recipeIds);
    $recipeIngridients=getIngridients($recipeIds);
    echo json_encode(fillRecipes($recipes, $recipeTags, $recipeIngridients));
}

/**
 *
 */
function addRecipe() {
    $sql = "INSERT INTO recipe (title, content) VALUES (?, ?)";
    try {
        $request = Flight::request();
        $recipe = json_decode($request->getBody());
        $db = connect_db();
        $stmt = $db->prepare($sql);
        $stmt->bind_param('ss', $recipe->title, $recipe->content);
        $stmt->execute();
        $stmt->close();
        $recipe->id = $db->insert_id;
        if (isset($recipe->tags) && !empty($recipe->tags)) {
            saveTags($recipe->tags, $recipe->id);
        }
        if (isset($recipe->ingridients) && !empty($recipe->ingridients)) {
            saveIngridients($recipe->ingridients, $recipe->id);
        }
        echo json_encode($recipe->id);
    } catch(Exception $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
};

/**
 *
 */
function editRecipe() {
    $request = Flight::request();
    $recipe = json_decode($request->getBody());
    $recipe->status = 0;
    $sql = "UPDATE recipe SET title = ?, content = ?, status = ? WHERE id = ?";
    try {
        $db = connect_db();
        $stmt = $db->prepare($sql);
        $stmt->bind_param('ssii', $recipe->title, $recipe->content, $recipe->status, $recipe->id);
        $stmt->execute();
        $stmt->close();
        updateTags($recipe->tags, $recipe->id);
        updateIngridients($recipe->ingridients, $recipe->id);
        echo "true";
        return;
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
};

/**
 * @param $id
 */
function approveRecipe($id) {
    $sql = "UPDATE recipe SET status = 1 WHERE id = " . $id;
    try {
        $db = connect_db();
        $result = $db->query($sql) or trigger_error($db->error."[$sql]");
        echo json_encode($result);
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
};

/**
 * @param $id
 */
function rejectRecipe($id) {
    $sql = "UPDATE recipe SET status = -1 WHERE id = " . $id;
    try {
        $db = connect_db();
        $result = $db->query($sql) or trigger_error($db->error."[$sql]");
        echo json_encode($result);
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
};

/**
 * @param $id
 */
function deleteRecipe($id) {
    $sql = "DELETE FROM recipe WHERE id = $id";
    try {
        $db = connect_db();
        $result = $db->query($sql) or trigger_error($db->error."[$sql]");
        deleteTagsByRecipeId($id);
        deleteIngridientsByRecipeId($id);
        echo json_encode($result);
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

// Тестовые методы, потом удалить.
Flight::route('GET /insertTestData', function () {
    $db = connect_db();
    for ($x = 0; $x <= 10; $x++) {
        $result = $db->query( 'INSERT INTO recipe (title,content) VALUES (\'Пример рецепта нумер'.$x.'\',\'Охапка дров и плов готов! Нумер '.$x.'\');');
    }
    echo "Ok, i have insert test data.";
});

Flight::route('GET /deleteTestData', function () {
    $db = connect_db();
    $result = $db->query( 'DELETE FROM recipe') or trigger_error($db->error."[.]");
    $result = $db->query( 'DELETE FROM tag') or trigger_error($db->error."[.]");
    $result = $db->query( 'DELETE FROM ingridient') or trigger_error($db->error."[.]");
    echo "All datas deleted!";
});


Flight::start();