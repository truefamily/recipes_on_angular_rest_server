<?php
include_once 'ChromePhp.php';
require_once 'utils.php';

function getRecipeTags($id) {
    $tagArray = array();
    $sqlTags = "SELECT tag.id, tag.name, tag2recipe.recipeId FROM tag, tag2recipe WHERE tag.id = tag2recipe.tagId AND tag2recipe.recipeId = $id";
    $db = connect_db();
    $result = $db->query($sqlTags) or trigger_error($db->error."[$sqlTags]");
    while($row = $result->fetch_array(MYSQL_ASSOC)) {
        $tagArray[] = $row;
    }
    return $tagArray;
}

function getTags($recipeIds) {
    $tagArray = array();
    $inQuery = concatenateForInQuery($recipeIds);
    $sqlTags = "SELECT tag.id, tag.name, tag2recipe.recipeId FROM tag, tag2recipe WHERE tag.id = tag2recipe.tagId AND tag2recipe.recipeId IN ($inQuery)";
    $db = connect_db();
    $result = $db->query($sqlTags) or trigger_error($db->error."[$sqlTags]");
    while($row = $result->fetch_array(MYSQL_ASSOC)) {
        $tagArray[] = $row;
    }
    //ChromePhp::log($tagArray);
    return $tagArray;
}

function saveTags($tags, $recipeId) {
    $db = connect_db();
    $sqlSaveRecipeTags = "INSERT INTO tag2recipe (recipeId, tagId)  VALUES (?,?);";
    $tagIds = array();
    try {
        foreach ($tags as $key => $value) {
            $sqlSaveTagInsert = "INSERT IGNORE INTO tag (name) VALUES ('$value->name');";
            $sqlSaveTagSelect = "SELECT id FROM tag WHERE name = '$value->name';";
            $db->query($sqlSaveTagInsert) or trigger_error($db->error."[$sqlSaveTagInsert]");
            $result = $db->query($sqlSaveTagSelect) or trigger_error($db->error."[$sqlSaveTagSelect]");
            $row = $result->fetch_assoc();
            $tagIds[] = $row['id'];
        }
        if (count($tagIds) > 0 && $stmtRecipeTag = $db->prepare($sqlSaveRecipeTags)) {
            foreach ($tagIds as $id) {
                $stmtRecipeTag->bind_param('ii', $recipeId, $id);
                $stmtRecipeTag->execute();
            }
            $stmtRecipeTag->close();
        }
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

function updateTags($tags, $recipeId) {
    deleteTagsByRecipeId($recipeId);
    saveTags($tags, $recipeId);
}

function deleteTagsByRecipeId($recipeId) {
    $sqlDeleteTags = "DELETE FROM tag2recipe WHERE recipeId = $recipeId";
    try {
        $db = connect_db();
        $result = $db->query($sqlDeleteTags) or trigger_error($db->error."[$sqlDeleteTags]");
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}