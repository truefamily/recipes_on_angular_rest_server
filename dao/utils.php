<?php
include_once 'ChromePhp.php';

function concatenateForInQuery($array) {
    $inQuery = "";
    foreach($array as $id)
    {
        $inQuery .= $id . ",";
    }
    //ChromePhp::log(rtrim($inQuery, ","));
    return rtrim($inQuery, ",");
}

function fillRecipes($recipes, $tags, $ingridients) {
    foreach($recipes as &$recipe) {
        $recipe['tags']=array();
        foreach($tags as $tag) {
            if($tag['recipeId']==$recipe['id']) {
                unset($tag['recipeId']);
                $recipe['tags'][]=$tag;
            }
        }
        $recipe['ingridients']=array();
        foreach($ingridients as $ingr) {
            if($ingr['recipeId']==$recipe['id']) {
                unset($ingr['recipeId']);
                $recipe['ingridients'][]=$ingr;
            }
        }
    }
    return $recipes;
}

function fillRecipe($recipe, $tags, $ingridients) {
    $recipe->tags=array();
    foreach($tags as $tag) {
        unset($tag['recipeId']);
        $recipe->tags[]=$tag;
    }
    $recipe->ingridients=array();
    foreach($ingridients as $ingr) {
        unset($ingr['recipeId']);
        $recipe->ingridients[]=$ingr;
    }
    return $recipe;
}