<?php

function fillMeasures() {
    $measures = array('гр', 'мл','шт','ч.л.','ст.л.','ст.','кг','л','пучок(-ка)','зубчик(-а)','перо(-а)','стебель(-я)','по вкусу');

    $sql = "INSERT INTO measure (name) VALUES (?)";
    try {
        foreach($measures as $measure)
        {
            echo $measure;
            $db = connect_db();
            if ($stmt = $db->prepare($sql)) {
                $stmt->bind_param('s', $measure);
            }
            else {
                printf("Errormessage: %s\n", $db->error);
            }
            $stmt->execute();
            $stmt->close();
        }
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}