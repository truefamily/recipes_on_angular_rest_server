<?php

/**
 *
 */
function getMeasures() {
    $sql = 'SELECT * FROM measure ORDER BY id ASC';
    $db = connect_db();
    $result = $db->query($sql) or trigger_error($db->error."[$sql]");
    $myArray = array();
    while($row = $result->fetch_array(MYSQL_ASSOC)) {
        $myArray[] = $row;
    }
    echo json_encode($myArray);
};

/**
 * @param $id
 * @return array
 */
function getMeasure($id) {
    $sql = 'SELECT * FROM measure WHERE measure.id='.$id;
    $db = connect_db();
    $result = $db->query($sql) or trigger_error($db->error."[$sql]");
    $measure=$result->fetch_object();
    return $measure;
}