<?php
include_once 'ChromePhp.php';
require_once 'utils.php';

/**
 * @param $id
 * @return array
 */
function getRecipeIngridients($id) {
    $ingrArray = array();
    $sqlIngridients = "SELECT ingridient.recipeId, ingridient.id, ingridient.name, ingridient.volume, measure.id AS m_id, measure.name AS m_name
                      FROM ingridient, measure WHERE ingridient.measureId = measure.id AND ingridient.recipeId = $id";
    $db = connect_db();
    $result = $db->query($sqlIngridients) or trigger_error($db->error."[$sqlIngridients]");
    while($row = $result->fetch_array(MYSQL_ASSOC)) {
        $measure = array('id' => $row['m_id'], 'name' => $row['m_name']);
        unset($row['m_id']);
        unset($row['m_name']);
        $row['measure']=$measure;
        $ingrArray[] = $row;
    }
    return $ingrArray;
}

function getIngridients($recipeIds) {
    $ingrArray = array();
    $inQuery = concatenateForInQuery($recipeIds);
    $sqlIngridients = "SELECT ingridient.recipeId, ingridient.id, ingridient.name, ingridient.volume, measure.id AS m_id, measure.name AS m_name
                      FROM ingridient, measure WHERE ingridient.measureId = measure.id AND ingridient.recipeId IN ($inQuery)";
    $db = connect_db();
    $result = $db->query($sqlIngridients) or trigger_error($db->error."[$sqlIngridients]");
    while($row = $result->fetch_array(MYSQL_ASSOC)) {
        $measure = array('id' => $row['m_id'], 'name' => $row['m_name']);
        unset($row['m_id']);
        unset($row['m_name']);
        $row['measure']=$measure;
        $ingrArray[] = $row;
        //ChromePhp::log($row);
    }
    return $ingrArray;
}

/**
 * @param $ingridients
 * @param $recipeId
 */
function saveIngridients($ingridients, $recipeId) {
    $sqlSaveIngridients = "INSERT INTO ingridient (recipeId, name, volume, measureId) VALUES (?,?,?,?)";
    try {
        $db = connect_db();
        foreach($ingridients as $key => $value)
        {
            if ($stmt = $db->prepare($sqlSaveIngridients)) {
                $stmt->bind_param('isdi', $recipeId, $value->name, $value->volume, $value->measure->id);
            }
            else {
                printf("Errormessage: %s\n", $db->error);
            }
            $stmt->execute();
            $stmt->close();
        }
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

function updateIngridients($ingridients, $recipeId) {
    deleteIngridientsByRecipeId($recipeId);
    saveIngridients($ingridients, $recipeId);
}

function deleteIngridientsByRecipeId($recipeId) {
    $sqlDeleteIngridients = "DELETE FROM ingridient WHERE recipeId = $recipeId";
    try {
        $db = connect_db();
        $result = $db->query($sqlDeleteIngridients) or trigger_error($db->error."[$sqlDeleteIngridients]");
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}